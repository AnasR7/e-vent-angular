<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

# E-vent Apps.

<!-- Dev logs Segment. -->
## Dev logs.

<!-- Backend log Segment. -->
### BackEnd log.
[ ] => Make sure the Tester Apps 100% working. 
[ ] => Resolve Problem in DB Connection with viper to PostgreSQL Database Server.
[ ] => Resolve how to Dockerize the Apps.
[ ] => Create Unit Testing.
[ ] => Create Real Apps.
