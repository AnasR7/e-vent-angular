///////////////////////////////////////
// This is a TEST for GIN Framework //
/////////////////////////////////////


package main

// Import modular
import (
	"net/http"
	"github.com/gin-gonic/gin"
	// "errors"
)


// --> ORM-Book
type book struct {
	ID       string `json:"id"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Quantity int `json:""`
}

// --> Create API Book Variable
var books = []book{
	{ID:"1", Title:"Mein Kaf", Author:"Adolf Hitler", Quantity: 13},
	{ID:"2", Title:"MADILOG", Author:"Tan Malaka", Quantity: 7},
	{ID:"3", Title:"Catatan Harian Gie", Author:"Soe Hoek Gie", Quantity: 22},
	{ID:"4", Title:"Hanya Malaikat Yang Tau", Author:"Terre Liye", Quantity: 3},
	{ID:"5", Title:"Djakarta 1945", Author:"Dr.Budhiarjho", Quantity: 12},
}

// Crate connect to get API books
func getBooks(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, books)
}

// Create func main
func main() {
	router := gin.Default()
	router.GET("/books", getBooks)
	router.Run("localhost:8080")
}
